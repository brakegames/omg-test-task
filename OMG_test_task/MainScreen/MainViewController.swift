//
//  MainViewController.swift
//  OMG_test_task
//
//  Created by EGOR TREPOV on 13.03.2024.
//

import UIKit

struct ItemIndex {
    var row = 0
    var pos = 0
}

struct Item {
    var index = ItemIndex(row: 0, pos: 0)
    var value = 0
}

class MainViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    var itemsList:[[Item]] = []
    
    var updateTimer = Timer()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.tableFooterView = UIView()
        
        self.initializeItemList()
        
        self.updateTimer = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { _ in
            self.randomize()
        })
    }
    
    func initializeItemList() {
        let numberOfRows = Int.random(in: 100..<1000)
        for row in 0...numberOfRows {
            var rowList:[Item] = []
            let numberOfItems = Int.random(in: 10..<100)
            for pos in 0...numberOfItems {
                let item = Item(index: ItemIndex(row: row, pos: pos), value: Int.random(in: 0..<100))
                rowList.append(item)
            }
            self.itemsList.append(rowList)
        }
        self.tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.itemsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tableCell") as! ContentTableViewCell
        cell.updateCell(list: self.itemsList[indexPath.row])
        return cell
    }
    
    func randomize() {
        let cell = self.tableView.visibleCells.randomElement() as! ContentTableViewCell
        let itemCell = cell.collectionView.visibleCells.randomElement() as! ItemCell
        let row = cell.rowList[0].index.row
        let pos = itemCell.pos
        var item = self.itemsList[row][pos]
        item.value = Int.random(in: 0..<100)
        self.itemsList[row][pos] = item
        cell.updateCell(list: self.itemsList[row], resetScroll: false)
    }

}
