//
//  ContentTableViewCell.swift
//  OMG_test_task
//
//  Created by EGOR TREPOV on 13.03.2024.
//

import UIKit

class ContentTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var rowList:[Item] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateCell(list: [Item], resetScroll: Bool = true) {
        self.rowList = list
        
        self.collectionView.reloadData()
        
        if resetScroll {
            self.collectionView.scrollToItem(
                at: IndexPath(item: 0, section: 0),
                at: .centeredHorizontally,
                animated: false
            )
        }
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return rowList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionCell", for: indexPath) as! ItemCell
        let item = rowList[indexPath.item]
        cell.mainView.layer.cornerRadius = 8
        cell.valueLabel.text = "\(item.value)"
        cell.pos = indexPath.item
        return cell
    }
}
