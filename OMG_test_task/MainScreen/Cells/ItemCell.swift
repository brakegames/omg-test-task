//
//  ItemCell.swift
//  OMG_test_task
//
//  Created by EGOR TREPOV on 13.03.2024.
//

import UIKit

class ItemCell: UICollectionViewCell {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var valueLabel: UILabel!
    
    var pos = 0
    
    @IBAction func touchDown(_ sender: UIButton) {
        UIView.animate(withDuration: 0.3) {
            self.mainView.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        }
    }
    
    @IBAction func touchUp(_ sender: UIButton) {
        self.upscaleAnimation()
    }
    
    @IBAction func touchUpOutside(_ sender: UIButton) {
        self.upscaleAnimation()
    }
    
    func upscaleAnimation() {
        UIView.animate(withDuration: 0.3) {
            self.mainView.transform = CGAffineTransform(scaleX: 1, y: 1)
        }
    }
}
